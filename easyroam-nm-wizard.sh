#!/usr/bin/env bash

###
# Only modification made to this is that I have disabled the geteduroam.sh scripts
# due to the fact that they no longer you to download eap-config automatically.
# Also warning about user not being in networkmanager group
# Original file: https://gitlab.rlp.net/rzht-public/easyroam-nm-wizard
###

set -e

export LANG=C

case "$1" in
-h|--help)
	echo "$0 [<wlan-if>] [<connection>]" >&2
	exit;;
esac

wlan_if="$1"
connection="${2:-Easyroam}"

# ---------------------------------------
# General checks
# ---------------------------------------

# check for nmcli
if ! type nmcli >/dev/null 2>&1; then
	echo "ERROR: nmcli not found!" >&2
	echo "This wizard assumes that your network connections are managed by NetworkManager." >&2
	exit 1
fi

# check if belong to group
if ! id -nG "$USER" | grep -qw "networkmanager"; then
	echo "WARNING: You may encounter problems by not being in the networkmanager group." >&2
	echo "Please add your user to the group if you encountered permission issues" >&2
fi

# check prerequisites
for d in openssl xmlstarlet; do
	type "$d" >/dev/null 2>&1 && continue
	echo "ERROR: $d not found!" >&2
	exit 2
done

# ---------------------------------------
# Find wifi device
# ---------------------------------------

if [ -z "$wlan_if" ]; then
	wlan_if=$(nmcli -g TYPE,DEVICE device | awk -F: '/^wifi:/{print $2}')
fi
if [ -z "$wlan_if" ]; then
	echo "ERROR: Unable to find any wifi device!" >&2
	exit 3
fi

# ---------------------------------------
# Persistent config files
# ---------------------------------------

         conf_dir="$HOME/.local/share/easyroam"
 client_cert_file="$conf_dir/easyroam_client_cert.pem"
  client_key_file="$conf_dir/easyroam_client_key.pem"
     root_ca_file="$conf_dir/easyroam_root_ca.pem"
  eap_config_file="$conf_dir/easyroam.eap-config"

[ -d "$conf_dir" ] || mkdir -p "$conf_dir"

# EAP Config file
if [ ! -f $eap_config_file ]; then
	echo "Please login into https://easyroam.de and  manually generate a *EAP Config* file."
	echo "After that, please move it to $eap_config_file"
	exit 1
fi

# ---------------------------------------
# Tools
# ---------------------------------------

# xmlstarlet shortcut
getvalue() {
	xmlstarlet sel -t -m "EAPIdentityProviderList/EAPIdentityProvider/$1" -v . "$eap_config_file"
}

# ---------------------------------------
# Extract values from eap_config file
# ---------------------------------------

             SSID=$(getvalue "CredentialApplicability/IEEE80211/SSID")

    OuterIdentity=$(getvalue "AuthenticationMethods/AuthenticationMethod/ClientSideCredential/OuterIdentity")
ClientCertificate=$(getvalue "AuthenticationMethods/AuthenticationMethod/ClientSideCredential/ClientCertificate")
       Passphrase=$(getvalue "AuthenticationMethods/AuthenticationMethod/ClientSideCredential/Passphrase")

               CA=$(getvalue "AuthenticationMethods/AuthenticationMethod/ServerSideCredential/CA")

# ---------------------------------------
# check openssl version 
# ---------------------------------------
openssl_extra=
version=$(openssl version | awk -F "[ .]" '{print $2}')
[ "${version:-2}" -ge 3 ] && openssl_extra="-legacy"

# ---------------------------------------
# main
# ---------------------------------------

echo -n "Extracting client cert ... "
base64 -d <<<"$ClientCertificate" | \
	openssl pkcs12 $openssl_extra -passin "pass:$Passphrase" -nokeys -out "$client_cert_file"
echo success

echo -n "Extracting client key ... "
base64 -d <<<"$ClientCertificate" | \
	openssl pkcs12 $openssl_extra -passin "pass:$Passphrase" -passout "pass:$Passphrase" -nodes -nocerts | \
	openssl rsa -passout "pass:$Passphrase" -aes128 -out "$client_key_file"

echo -n "Extracting CA cert ... "
base64 -d <<<"$CA" | openssl x509 -inform DER -out "$root_ca_file"
echo success

# Remove existing connections
nmcli connection show | \
	awk '$1==c{ print $2 }' c="$connection" | \
	xargs -rn1 nmcli connection delete uuid

# Create new connection
nmcli connection add \
	type wifi \
	con-name "$connection" \
	ifname "$wlan_if" \
	ssid "$SSID" \
	-- \
	wifi-sec.key-mgmt wpa-eap \
	802-1x.eap tls \
	802-1x.identity "$OuterIdentity" \
	802-1x.ca-cert "$root_ca_file" \
	802-1x.client-cert "$client_cert_file" \
	802-1x.private-key-password "$Passphrase" \
	802-1x.private-key "$client_key_file"
